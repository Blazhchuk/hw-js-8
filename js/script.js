"use strict"

/*

1.  Метод forEach() виконує вказану фунцію один раз для кожного елемента, при цьому не змінюючи сам масив

2.  Методи які мутують масив: push(), pop(), shift(), unshift(), splice(), reverse().
    Методи які повертають новий масив: map(), filter(), slice(), concat().

    let arr = [1, 2, 3, 4, 5];

    - Методи які мутують масив:
    arr.push('string', 9); // arr стає [1, 2, 3, 4, 5, 'string', 9]
    arr.pop(); // arr стає [1, 2, 3, 4, 5, 'string']
    arr.shift(); // arr стає [2, 3, 4, 5, 'string']

    - Методи які повертають новий масив:
    let newArr = arr.map(x => x * 2); // newArr = [4, 6, 8, 10, NaN]
    let filterArr = arr.filter(x => x > 2); // filterArr = [3, 4, 5]
    let concatArr = arr.concat([6, 7, 10]); concatArr = [2, 3, 4, 5, 'string', 6, 7, 10]

3.  Метод Array.isArray() перевіряє чи є змінна масивом, повертає boolean

4.  Вибір між forEach() та map() залежить від того, чи потрібно стоворити новий масив, чи виконати оперцію в існуючому масиві 

*/


// ex 1

let arrString = [
    "travel",
    "hello",
    "eat",
    "ski",
    "lift"
];

let count = arrString.filter(word => word.length > 3).length;

// console.log(count);

arrString.forEach(word => {
    if (word.length > 3) {
        // console.log(word);
    }
});


// ex 2

let userInfo = [
    { name: "Михайло", age: 22, sex: "чоловіча" },
    { name: "Аня", age: 18, sex: "жіноча" },
    { name: "Андрій", age: 24, sex: "чоловіча" },
    { name: "Валерія", age: 31, sex: "жіноча" }
];

let filterSex = userInfo.filter(gender => gender.sex === "чоловіча");

// console.log(filterSex);


// ex 3

function filterBy(arr, type) {
    return arr.filter(item => typeof item !== type);
}

let kitArr = [
    'hello',
    'world',
    23,
    '23',
    null
];

let filtFunc = filterBy(kitArr, 'string');

console.log(filtFunc);
